# frozen_string_literal: true

require 'json'

require_relative '../../triage/processor'
require_relative '../../triage/event'
require_relative '../../triage/pipeline_jobs_status_manager'
require_relative '../../triage/pipeline_failure/config_selector'
require_relative '../../triage/pipeline_failure/config/master_branch'

module Triage
  class UpdateMasterPipelineJobsStatus < Processor
    PIPELINE_STATUS_FILE = 'canonical-gitlab-master-pipeline-status.json'
    PIPELINE_BRANCH_NAME = 'master'
    STATUS_REPO_BRANCH_NAME = 'master-pipeline-status'
    STATUS_REPO_PROJECT_ID = Triage::Event::MASTER_BROKEN_INCIDENT_PROJECT_ID

    react_to 'pipeline.failed', 'pipeline.success'

    def applicable?
      event.from_gitlab_org_gitlab? && match_master_pipeline?
    end

    def process
      Triage::PipelineJobsStatusManager.new(
        api_client: api_client,
        project_id: STATUS_REPO_PROJECT_ID,
        file_name: PIPELINE_STATUS_FILE,
        branch_name: STATUS_REPO_BRANCH_NAME
      ).update_and_publish(
        jobs_payload: pipeline_jobs,
        commit_message: "Update #{PIPELINE_BRANCH_NAME} pipeline status for #{event.url}"
      )
    end

    def documentation
      <<~TEXT
        Updates job statuses after a master pipeline finishes in the canonical GitLab project.

        See https://gitlab.com/gitlab-org/quality/engineering-productivity/master-broken-incidents/-/blob/master-pipeline-status/canonical-gitlab-master-pipeline-status.json
      TEXT
    end

    private

    def api_client
      @api_client ||= Triage.api_client(event.instance)
    end

    def pipeline_jobs
      api_client.pipeline_jobs(event.project_id, event.id, per_page: 100).auto_paginate
    end

    def match_master_pipeline?
      !!PipelineFailure::ConfigSelector.find_config(event, [Triage::PipelineFailure::Config::MasterBranch])
    end
  end
end
