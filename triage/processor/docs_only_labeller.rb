# frozen_string_literal: true

require 'digest'

require_relative '../triage/changed_file_list'
require_relative '../job/docs_only_labeller_job'
require_relative '../triage/reaction'
require_relative '../triage/processor'
require_relative '../triage/event'

module Triage
  # Allows any community contributor to ask for a review.
  # The automation will pick a random MR coach and ask them to review the MR.
  class DocsOnlyLabeller < Processor
    FIVE_MINUTES = 60 * 5
    PROJECTS_WITH_DOCS = [
      Triage::Event::GITLAB_PROJECT_ID,
      Triage::Event::OMNIBUS_PROJECT_ID,
      Triage::Event::RUNNER_PROJECT_ID,
      Triage::Event::CHART_PROJECT_ID,
      Triage::Event::OPERATOR_PROJECT_ID,
      Triage::Event::DOCS_PROJECT_ID
    ].freeze

    react_to 'merge_request.update'

    def applicable?
      event.resource_open? &&
        PROJECTS_WITH_DOCS.include?(event.project_id)
    end

    def process
      DocsOnlyLabellerJob.perform_in(FIVE_MINUTES, event)
    end

    def documentation
      <<~TEXT
      This command enques a job which will fire once the diff calculation is ready.

      The job will add or remove the `~docs-only` label depending on the changes.
      TEXT
    end
  end
end
