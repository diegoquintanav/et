# frozen_string_literal: true

require_relative 'base'

module Triage
  module PipelineFailure
    module Config
      class RubyBranch < Base
        SLACK_CHANNEL = 'f_ruby3'
        APPLICABLE_PROJECT_PATHS = ['gitlab-org/gitlab', 'gitlab-org/gitlab-foss'].freeze

        def self.match?(event)
          event.on_instance?(:com) &&
            APPLICABLE_PROJECT_PATHS.include?(event.project_path_with_namespace) &&
            event.ref.match?(/\Aruby\d+(_\d)*\z/) &&
            event.source_job_id.nil?
        end

        def default_slack_channels
          [SLACK_CHANNEL]
        end
      end
    end
  end
end
