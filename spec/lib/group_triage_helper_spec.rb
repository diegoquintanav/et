# frozen_string_literal: true

require 'spec_helper'
require_relative '../../lib/group_triage_helper'
require_relative '../../lib/lazy_heat_map'

RSpec.describe GroupTriageHelperContext do
  let(:test_helper) do
    Class.new do
      include GroupTriageHelperContext

      def network
        Struct.new(:options).new(Struct.new(:source, :source_id).new('projects', 'gitlab-org/gitlab'))
      end

      def titled_heatmap(title, _options = {})
        "Stub heatmap with title: #{title}"
      end

      def resource; end
    end
  end

  let(:instance) { test_helper.new }

  describe '#merge_requests_needing_attention_report' do
    subject { instance.merge_requests_needing_attention_report(group_key: 'group1') }

    it_behaves_like 'report summaries', "`@fake_be_em` `@fake_em` `@fake_fe_em` `@fake_fs_em`" do
      describe 'due date' do
        it 'sets a due date' do
          expect(subject).to match("/due #{described_class::REPORT_DUE}")
        end
      end

      describe 'value stream analytics link' do
        it 'includes the VSA text' do
          expect(subject).to include('Additionally, you can see the long running issues and merge requests')
        end

        it 'includes the group label in the link' do
          expect(subject).to include('label_name[]=group%3A%3Agroup1')
        end
      end
    end
  end

  describe '#report_title' do
    around do |example|
      Timecop.freeze(2019, 11, 1) { example.run }
    end

    before do
      allow(instance).to receive(:source_name).and_return('gitlab-org/gitlab')
    end

    subject { instance.report_title('group1') }

    it 'returns title' do
      expect(subject).to eq("2019-11-01 - Triage report for \"group1\" - gitlab-org/gitlab")
    end
  end

  describe '#short_team_summary' do
    subject { instance.short_team_summary(group_key: "group1", title: title, extra_assignees: ['@joe']) }

    let(:title) { 'A very specific title' }

    it_behaves_like 'report summaries', "`@fake_be_em` `@fake_em` `@fake_fe_em` `@fake_fs_em` `@fake_pm` `@joe`"

    context 'when a title is provided' do
      include_context 'with report metadata'

      it 'shows the title in the description' do
        expect(subject).to match(title)
      end
    end
  end

  describe "#report_summary" do
    subject { instance.report_summary(group_key: "group1") }

    it_behaves_like 'report summaries'
  end

  describe "#security_heatmap" do
    it 'returns a heatmap' do
      expect(instance.security_heatmap).to eq('Stub heatmap with title: #### Heatmap for security vulnerability issues')
    end
  end

  describe "#availability_heatmap" do
    it 'returns a heatmap' do
      expect(instance.availability_heatmap).to eq('Stub heatmap with title: #### Heatmap for all availability issues')
    end
  end

  describe "#infradev_all_issues_heatmap" do
    it 'returns a heatmaps' do
      expect(instance.infradev_all_issues_heatmap).to eq('Stub heatmap with title: ### Heatmap for all ~infradev issues')
    end
  end

  describe "#bug_heatmap" do
    it 'returns a heatmap' do
      expect(instance.bug_heatmap).to eq('Stub heatmap with title: #### Heatmap for all bugs')
    end
  end

  describe "#blocked_bugs_heatmap" do
    it 'returns a heatmap' do
      expect(instance.blocked_bugs_heatmap).to eq('Stub heatmap with title: ### Heatmap for blocked bugs')
    end
  end

  describe "#vintage_heatmap" do
    it 'returns a heatmap' do
      expect(instance.vintage_heatmap).to eq('Stub heatmap with title: ### Heatmap for ~vintage bugs')
    end
  end

  describe "#customer_bugs_heatmap" do
    it 'returns a heatmap' do
      expect(instance.customer_bugs_heatmap).to eq('Stub heatmap with title: ### Heatmap for ~customer bugs')
    end
  end

  describe "#flaky_tests_heatmap" do
    it 'returns a heatmap' do
      expect(instance.flaky_tests_heatmap).to eq('Stub heatmap with title: ### Heatmap for ~"failure::flaky-test" bugs')
    end
  end

  describe "#ux_debt_heatmap" do
    it 'returns a heatmap' do
      expect(instance.ux_debt_heatmap).to eq('Stub heatmap with title: #### Heatmap for all UX debt')
    end
  end

  describe "#ui_polish_heatmap" do
    it 'returns a heatmap' do
      expect(instance.ui_polish_heatmap).to eq('Stub heatmap with title: #### Heatmap for all UI polish')
    end
  end

  describe "#build_command" do
    let(:strings) { %w[user1 user2 user3] }

    context 'with no flags' do
      subject { instance.build_command(strings) }

      it 'returns the correct string' do
        expect(subject).to eq("user1 user2 user3")
      end
    end

    context 'with a prefix' do
      subject { instance.build_command(strings, prefix: 'hey-') }

      it 'returns the correct string' do
        expect(subject).to eq("hey-user1 hey-user2 hey-user3")
      end
    end

    context 'with the quote flag' do
      subject { instance.build_command(strings, quote: true) }

      it 'returns the correct string' do
        expect(subject).to eq("\"user1\" \"user2\" \"user3\"")
      end
    end

    context 'with the backticks flag' do
      subject { instance.build_command(strings, backticks: true) }

      it 'returns the correct string' do
        expect(subject).to eq("`user1` `user2` `user3`")
      end
    end

    context 'with several flags' do
      subject { instance.build_command(strings, prefix: 'a-', quote: true, backticks: true) }

      it 'returns the correct string' do
        expect(subject).to eq("`a-\"user1\"` `a-\"user2\"` `a-\"user3\"`")
      end
    end
  end

  describe "#build_mentions" do
    let(:assignees) { %w[user1 user2 user3] }

    subject { instance.build_mentions(assignees) }

    it 'returns a string of assignees' do
      expect(subject).to eq("@user1 @user2 @user3")
    end
  end

  describe "#build_backticked_mentions" do
    let(:assignees) { %w[user1 user2 user3] }

    subject { instance.build_backticked_mentions(assignees) }

    it 'returns a string of backticked assignees' do
      expect(subject).to eq("`@user1` `@user2` `@user3`")
    end
  end

  describe "#missed_slo_heatmap" do
    let(:heat_map) { instance_double(LazyHeatMap) }

    before do
      allow(heat_map).to receive(:generate_heat_map_table_stuck).and_return("fake-data-stuck-issues")
      allow(instance).to receive(:resource).and_return(heat_map: heat_map)
    end

    it 'returns two separate heatmaps' do
      expect(instance.missed_slo_heatmap).to eq(
        <<~HEATMAP
          Stub heatmap with title: ### Heatmap for ~SLO::Missed bugs

          ### Heatmap for ~SLO::Missed bugs (stuck issues)

          fake-data-stuck-issues

          ----

        HEATMAP
      )
    end
  end

  describe '#default_greetings' do
    subject { instance.default_greetings(assignees) }

    context 'when the assignees are nil' do
      let(:assignees) { nil }

      it 'returns an empty string' do
        expect(subject).to be_empty
      end
    end

    context 'when the assignees are empty' do
      let(:assignees) { [] }

      it 'returns an empty string' do
        expect(subject).to be_empty
      end
    end

    context 'when there are assignees' do
      let(:assignees) { %w[@user1 @user2] }

      it 'returns a greeting for the assignees (backticked)' do
        expect(subject).to eq('Hi `@user1` `@user2`,')
      end
    end
  end
end
